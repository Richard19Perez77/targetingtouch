package android.target;

import on.draw.Draw;
import on.draw.DrawFactory;
import on.touch.OnTouch;
import on.touch.OnTouchFactory;
import update.physics.UpdatePhysics;
import update.physics.UpdatePhysicsFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class Targeting {

	public int screenWidth, screenHeight;
	public int top, right, left, bottom;
	public Paint targetHomingPaint = new Paint();
	public Paint targetObjectPaint = new Paint();
	public int targetPixel;
	public float STROKE_WIDTH = 10;
	public boolean isTouchedOnTime = false;
	public boolean isTouched;
	public boolean isRoundOver;
	public boolean touchUpdated;
	public boolean isTouchTimingSet;

	String currentOnTouch = "a";
	OnTouch onTouch = OnTouchFactory.createTouch(currentOnTouch);

	String currentPhysics = "c";
	UpdatePhysics updatePhysics = UpdatePhysicsFactory
			.createUpdatePhysics(currentPhysics);

	String currentDraw = "a";
	Draw draw = DrawFactory.createDraw(currentDraw);

	public int startTargetPixel;

	public Target homingTarget, homeTarget;
	boolean targetsReady;

	public class Target {
		public Rect rect;
	}

	public Targeting() {
		targetHomingPaint.setColor(Color.WHITE);
		targetHomingPaint.setStyle(Style.STROKE);
		targetHomingPaint.setStrokeWidth(1);

		targetObjectPaint.setColor(Color.BLACK);
		targetObjectPaint.setStyle(Style.STROKE);
		targetObjectPaint.setStrokeWidth(STROKE_WIDTH);
	}

	public void draw(Canvas canvas) {
		draw.draw(canvas, this);
	}

	public void surfaceChanged(SurfaceHolder holder, int height, int width) {
		// called when the surface is created for the thread
		bottom = screenHeight = height;
		right = screenWidth = width;
		left = top = 0;

		// set the initial target location and increment amount
		startTargetPixel = targetPixel = screenWidth / 20;
	}

	/**
	 * Perform click needs to be implemented
	 */
	public boolean onTouch(MotionEvent event) {
		// touch tracks the life cycle of the touch event
		return onTouch.onTouch(event, this);
	}

	public void updatePhysics() {
		if (targetsReady) {
			homingTarget = new Target();
			homingTarget.rect = new Rect();
			homingTarget.rect.top = 0;
			homingTarget.rect.left = 0;
			homingTarget.rect.right = screenWidth;
			homingTarget.rect.bottom = screenHeight;

			homeTarget = new Target();
			homeTarget.rect = new Rect();
			homeTarget.rect.top = startTargetPixel;
			homeTarget.rect.left = startTargetPixel;
			homeTarget.rect.right = screenWidth - startTargetPixel;
			homeTarget.rect.bottom = screenHeight - startTargetPixel;
		}
		updatePhysics.updatePhysics(this);
		if (updatePhysics.getRoundStatus()) {
			currentPhysics = "win";
			updatePhysics = UpdatePhysicsFactory
					.createUpdatePhysics(currentPhysics);
			currentDraw = "win";
			draw = DrawFactory.createDraw(currentDraw);
		}
	}

	public void menuRestart() {
		currentPhysics = "c";
		updatePhysics = UpdatePhysicsFactory
				.createUpdatePhysics(currentPhysics);
		currentDraw = "a";
		draw = DrawFactory.createDraw(currentDraw);

		bottom = screenHeight;
		right = screenWidth;
		left = top = 0;

		// set the initial target location and increment amount
		targetPixel = startTargetPixel;

		isTouched = false;
		isTouchedOnTime = false;
		isTouchTimingSet = false;
		touchUpdated = false;
		isRoundOver = false;
	}
}