package android.target;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends Activity {

	public DrawingSurface drawingSurface;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		drawingSurface = (DrawingSurface) findViewById(R.id.drawingSurface);
		drawingSurface.setTextView((TextView) findViewById(R.id.text));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.restart:
			drawingSurface.menuRestart();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		drawingSurface.onResume();
	}

	@Override
	protected void onStart() {
		super.onStart();
		drawingSurface.onStart();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		drawingSurface.onRestart();
	}

	@Override
	protected void onPause() {
		super.onPause();
		drawingSurface.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		drawingSurface.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		drawingSurface.onDestroy();
	}
}