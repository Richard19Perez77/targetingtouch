package update.physics;

import android.target.Targeting;

public class UpdatePhysicsAImpl implements UpdatePhysics {

	@Override
	public void updatePhysics(Targeting targeting) {
		// called if application and thread are running
		if (targeting.isTouched && !targeting.touchUpdated) {
			targeting.touchUpdated = true;

			if (targeting.left > (targeting.targetPixel - targeting.STROKE_WIDTH)
					&& (targeting.left < targeting.targetPixel
							+ targeting.STROKE_WIDTH)) {
				targeting.isTouchedOnTime = true;
				targeting.isRoundOver = true;
				targeting.targetPixel += targeting.startTargetPixel;
			} else {
				targeting.isTouchedOnTime = false;
				targeting.isRoundOver = true;
				targeting.targetPixel -= targeting.startTargetPixel;
			}
		}

		targeting.left++;
		targeting.top++;
		targeting.right--;
		targeting.bottom--;

		if (targeting.left >= targeting.right
				|| targeting.top >= targeting.bottom) {
			targeting.isTouchedOnTime = false;
			targeting.isTouched = false;
			targeting.touchUpdated = false;
			targeting.isRoundOver = false;
			targeting.left = 0;
			targeting.right = targeting.screenWidth;
			targeting.top = 0;
			targeting.bottom = targeting.screenHeight;
		}
	}

	@Override
	public boolean getRoundStatus() {
		// TODO Auto-generated method stub
		return false;
	}
}