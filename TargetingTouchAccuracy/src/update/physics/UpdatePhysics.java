package update.physics;

import android.target.Targeting;

public interface UpdatePhysics {
	public void updatePhysics(Targeting targeting);

	public boolean getRoundStatus();
}
