package on.touch;

public class OnTouchFactory {
	OnTouchFactory() {
	};

	public static OnTouch createTouch(String type) {
		if (type.equals("a"))
			return new OnTouchAImpl();
		else
			return null;
	}
}
