package on.touch;

import android.target.Targeting;
import android.view.MotionEvent;

public class OnTouchAImpl implements OnTouch {

	@Override
	public boolean onTouch(MotionEvent event, Targeting targeting) {
		if (!targeting.isRoundOver && !targeting.isTouched) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				// touch event has happened
				targeting.isTouched = true;
				targeting.touchUpdated = false;
				return false;
			}
		}
		return true;
	}
}