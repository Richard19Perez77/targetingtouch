package on.touch;

import android.target.Targeting;
import android.view.MotionEvent;

public interface OnTouch {
	public boolean onTouch(MotionEvent event, Targeting targeting);
}
