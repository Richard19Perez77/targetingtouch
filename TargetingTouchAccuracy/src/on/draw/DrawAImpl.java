package on.draw;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.target.Targeting;

public class DrawAImpl implements Draw {

	@Override
	public void draw(Canvas canvas, Targeting targeting) {
		// TODO Auto-generated method stub
		if (!targeting.isTouchTimingSet)
			canvas.drawColor(Color.BLUE);
		else if (targeting.isTouchedOnTime)
			canvas.drawColor(Color.GREEN);
		else
			canvas.drawColor(Color.RED);

		canvas.drawRect(new Rect(targeting.targetPixel, targeting.targetPixel,
				targeting.screenWidth - targeting.targetPixel,
				targeting.screenHeight - targeting.targetPixel),
				targeting.targetObjectPaint);

		canvas.drawRect(new Rect(targeting.left, targeting.top,
				targeting.right - 1, targeting.bottom),
				targeting.targetHomingPaint);
		
		canvas.drawRect(targeting.homeTarget.rect, targeting.targetObjectPaint);
	}
}