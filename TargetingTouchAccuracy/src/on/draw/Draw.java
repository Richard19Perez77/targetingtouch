package on.draw;

import android.graphics.Canvas;
import android.target.Targeting;

public interface Draw {
	public void draw(Canvas canvas, Targeting targeting);
}
